execute pathogen#infect()

syntax on

filetype plugin on
set omnifunc=syntaxcomplete#Complete

set nu
set autoindent
set tabstop=4
set shiftwidth=4

colorscheme torte

if has('gui_running')
	set guifont=Terminus\ 14
	colorscheme solarized
	set cursorline
endif
